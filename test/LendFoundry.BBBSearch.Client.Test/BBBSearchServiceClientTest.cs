﻿using LendFoundry.Syndication.BBBSearch.Events;
using LendFoundry.Syndication.BBBSearch.Request;
using LendFoundry.Syndication.BBBSearch.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.BBBSearch.Client.Test
{
    public class BBBSearchServiceClientTest
    {
        #region Public Constructors

        public BBBSearchServiceClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            bbbSearchServiceClient = new BBBSearchService(MockServiceClient.Object);
        }

        #endregion Public Constructors

        #region Private Properties

        public BBBSearchService bbbSearchServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        #endregion Private Properties

        #region Public Methods

        [Fact]
        public async void client_SearchOrganization()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync<BBBSearchResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new BBBSearchResponse());

            var response = await bbbSearchServiceClient.SearchOrganization("123", "test", It.IsAny<IBBBSearchRequest>());
            Assert.Equal("/{entitytype}/{entityid}/bbbsearch/organization", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }

        #endregion Public Methods
    }
}