﻿using LendFoundry.BBBSearch.Api.Controller;
using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Syndication.BBBSearch.Request;
using LendFoundry.Syndication.BBBSearch.Response;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.BBBSearch.Api.Test
{
    public class BBBSearchControllerTest
    {
        private ApiController GetController(IBBBSearchService bbbSearchService)
        {
            return new ApiController(bbbSearchService);
        }

        private Mock<IBBBSearchService> GetBBBSearchService()
        {
            return new Mock<IBBBSearchService>();
        }

        [Fact]
        public async void SearchOrganizationReturnOnSuccess()
        {
            var bbbSearchService = GetBBBSearchService();
            bbbSearchService.Setup(x => x.SearchOrganization("", "", It.IsAny<IBBBSearchRequest>())).ReturnsAsync(new BBBSearchResponse());
            var response = (HttpOkObjectResult)await GetController(bbbSearchService.Object).SearchOrganization("", "", It.IsAny<BBBSearchRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void SearchOrganizationReturnError()
        {
            var bbbSearchService = GetBBBSearchService();
            bbbSearchService.Setup(x => x.SearchOrganization(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IBBBSearchRequest>())).Throws(new BBBSearchException());

            var response = (ErrorResult)await GetController(bbbSearchService.Object).SearchOrganization("123", "123", It.IsAny<BBBSearchRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}