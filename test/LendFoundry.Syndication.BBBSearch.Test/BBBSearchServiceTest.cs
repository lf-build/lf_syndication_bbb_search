﻿using LendFoundry.Syndication.BBBSearch.Proxy;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.BBBSearch.Test
{
    public class BBBSearchServiceTest
    {
        private Mock<IEventHubClient> EventHub { get; set; }
        private Mock<ILookupService> Lookup { get; set; }
        private IBBBSearchProxy Proxy { get; set; }

        private BBBSearchService bbbSearch { get; set; }
        private BBBSearchResponse bbbResponse { get; set; }

        public BBBSearchServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new BBBSearchProxy(new BBBSearchConfiguration
            {
                //simulation configuration
                AuthToken = "yDaZzqCPy1opO9iU3dB0DlJWxapG9RuZiUF1ecNEvb7-o-vb_k0kYleKJFejbEGreEl1_tskOfrAOSLD2-h6jSYQ2PA-_4gLfq4IUzbT9Zs69GMp1qb6rU9OKmWi7t0gVo8s4cTlXGygAqlww3GTl7lODb8GbMXZvPqPjK-g1NzkUMdmu2TyMl9R1DlmjSaAuCkWKAFcCxt5CKw2PmTMa2uOnQeGYImY2QOieaKFrziOtjxFlJTO29UJbHa5EUoQ-FiqfmPlmAkwYZYvLV5DuCXhj_MaVlLpMonvigQcg55I3SqPi2CeqW-0GsTCxirlA6y48pwD-sWPRj7KIjQujJts8WhbQ_8KeF_pEfTWJQafElxUsW-yy24irpJfM30z2KZg0fS5RzIOmHchwKet-uOhKxnuUl1gadOM0vy2tsuhR5eh3UE9JJ-sYKssSZWamGI3Xt4qMzp70DoO3gSqbjIbwsE-gp5CsSdwU9Ipn4HiK2EoYkvD3B4i4oYyJyyY16blMSWs5SrwyWrP7Kwyl58wPcHKGdPvPFucQAYXW-5U183uzdl6_RHYTLswaIPXPZcMVtPGEcxC_Y1lMbSY7R1CSt76n_wbKUEvDvphoeSd8efBiO7BEFudZ19aFJBLBG6RjtwPcMHQe1fYCp_CsfwEBl0",
                BaseUrl = "http://192.168.1.67:7022",
                OrganizationSearchUrl = "/BBBSearch/",
                UseProxy = false,
                ProxyUrl = "",

                //actual configuration
                //OrganizationSearchUrl = "/api/orgs/search",
                //BaseUrl = "https://api.bbb.org",
                //AuthToken = "yDaZzqCPy1opO9iU3dB0DlJWxapG9RuZiUF1ecNEvb7-o-vb_k0kYleKJFejbEGreEl1_tskOfrAOSLD2-h6jSYQ2PA-_4gLfq4IUzbT9Zs69GMp1qb6rU9OKmWi7t0gVo8s4cTlXGygAqlww3GTl7lODb8GbMXZvPqPjK-g1NzkUMdmu2TyMl9R1DlmjSaAuCkWKAFcCxt5CKw2PmTMa2uOnQeGYImY2QOieaKFrziOtjxFlJTO29UJbHa5EUoQ-FiqfmPlmAkwYZYvLV5DuCXhj_MaVlLpMonvigQcg55I3SqPi2CeqW-0GsTCxirlA6y48pwD-sWPRj7KIjQujJts8WhbQ_8KeF_pEfTWJQafElxUsW-yy24irpJfM30z2KZg0fS5RzIOmHchwKet-uOhKxnuUl1gadOM0vy2tsuhR5eh3UE9JJ-sYKssSZWamGI3Xt4qMzp70DoO3gSqbjIbwsE-gp5CsSdwU9Ipn4HiK2EoYkvD3B4i4oYyJyyY16blMSWs5SrwyWrP7Kwyl58wPcHKGdPvPFucQAYXW-5U183uzdl6_RHYTLswaIPXPZcMVtPGEcxC_Y1lMbSY7R1CSt76n_wbKUEvDvphoeSd8efBiO7BEFudZ19aFJBLBG6RjtwPcMHQe1fYCp_CsfwEBl0"
                // UseProxy = true,
                //ProxyUrl = "",
            });
            bbbResponse = new BBBSearchResponse();

            bbbSearch = new BBBSearchService(Proxy, EventHub.Object, Lookup.Object);
        }

        /// <summary>
        /// Searches the organization throws argument exception missing parameters
        /// </summary>
        [Fact]
        public void ThrowsArgumentExceptionWithWithMissingParameter_SearchOrganization()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => bbbSearch.SearchOrganization(null, "test", It.IsAny<Request.BBBSearchRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => bbbSearch.SearchOrganization("test", " ", It.IsAny<Request.BBBSearchRequest>()));
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("test", "test");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            bbbResponse = null;
            // Proxy.Setup(x => x.SearchOrganization(It.IsAny<BBBSearchRequest>())).Returns(bbbResponse);
            Assert.ThrowsAsync<ArgumentNullException>(() => bbbSearch.SearchOrganization("test", "test", It.IsAny<Request.BBBSearchRequest>()));
        }

        /// <summary>
        /// Success response with notnull
        /// </summary>
        [Fact]
        public void VerifySuccessWithNotNull_SearchOrganization()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var bbbSearchResponse = bbbSearch.SearchOrganization("application", "CA000001", new Request.BBBSearchRequest
            {
                PageSize = 10,
                PageNumber = 2,
                BusinessId = null,
                BBBId = null,
                BBBRating = null,
                IsBBBAccredited = false,
                IsReportable = false,
                OrganizationName = "Capital Alliance",
                PrimaryOrganizationName = null,
                PrimaryCategory = null,
                ContactFirstName = null,
                ContactLastName = null,
                ContactMiddleName = null,
                ContactTitle = null,
                ContactPrefix = null,
                Address = null,
                City = null,
                StateProvince = null,
                PostalCode = null,
                Phone = null,
                BusinessURL = null,
                ReportURL = null,
                OrganizationType = null,
                CollectionId = 0,
                OrganizationLastChanged = null,
                RatingLastChanged = null,
                AccreditationStatusLastChanged = null
            }).Result;
            Assert.NotNull(bbbSearchResponse.SearchResults);
            Assert.Equal(bbbSearchResponse.TotalResults, 148);
            Assert.Equal(bbbSearchResponse.SearchResults.Count, 10);
        }

        /// <summary>
        /// success with null response
        /// </summary>
        [Fact]
        public void VerifySuccessWithNULL_SearchOrganization()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var bbbSearchResponse = bbbSearch.SearchOrganization("application", "CA000001", new Request.BBBSearchRequest
            {
                PageSize = 10,
                PageNumber = 2,
                BusinessId = null,
                BBBId = null,
                BBBRating = null,
                IsBBBAccredited = false,
                IsReportable = false,
                OrganizationName = "xyz",
                PrimaryOrganizationName = null,
                PrimaryCategory = null,
                ContactFirstName = null,
                ContactLastName = null,
                ContactMiddleName = null,
                ContactTitle = null,
                ContactPrefix = null,
                Address = null,
                City = null,
                StateProvince = null,
                PostalCode = null,
                Phone = null,
                BusinessURL = null,
                ReportURL = null,
                OrganizationType = null,
                CollectionId = 0,
                OrganizationLastChanged = null,
                RatingLastChanged = null,
                AccreditationStatusLastChanged = null
            }).Result;
            Assert.Null(bbbSearchResponse.SearchResults);
            Assert.Equal(bbbSearchResponse.PageNumber, 0);
            Assert.Equal(bbbSearchResponse.TotalResults, 0);
        }
    }
}