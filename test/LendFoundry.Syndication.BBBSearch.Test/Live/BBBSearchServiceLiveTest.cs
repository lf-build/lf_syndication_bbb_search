﻿using LendFoundry.Syndication.BBBSearch.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.BBBSearch.Test.Live
{
    public class BBBSearchServiceLiveTest
    {
        private IBBBSearchProxy bbbSearchProxy { get; }
        private IBBBSearchConfiguration bbbSearchConfiguration { get; }

        public BBBSearchServiceLiveTest()
        {
            bbbSearchConfiguration = new BBBSearchConfiguration
            {
                OrganizationSearchUrl = "/BBBSearch/",
                BaseUrl = "http://192.168.1.67:7022",
                UseProxy= false,
                AuthToken = "yDaZzqCPy1opO9iU3dB0DlJWxapG9RuZiUF1ecNEvb7-o-vb_k0kYleKJFejbEGreEl1_tskOfrAOSLD2-h6jSYQ2PA-_4gLfq4IUzbT9Zs69GMp1qb6rU9OKmWi7t0gVo8s4cTlXGygAqlww3GTl7lODb8GbMXZvPqPjK-g1NzkUMdmu2TyMl9R1DlmjSaAuCkWKAFcCxt5CKw2PmTMa2uOnQeGYImY2QOieaKFrziOtjxFlJTO29UJbHa5EUoQ-FiqfmPlmAkwYZYvLV5DuCXhj_MaVlLpMonvigQcg55I3SqPi2CeqW-0GsTCxirlA6y48pwD-sWPRj7KIjQujJts8WhbQ_8KeF_pEfTWJQafElxUsW-yy24irpJfM30z2KZg0fS5RzIOmHchwKet-uOhKxnuUl1gadOM0vy2tsuhR5eh3UE9JJ-sYKssSZWamGI3Xt4qMzp70DoO3gSqbjIbwsE-gp5CsSdwU9Ipn4HiK2EoYkvD3B4i4oYyJyyY16blMSWs5SrwyWrP7Kwyl58wPcHKGdPvPFucQAYXW-5U183uzdl6_RHYTLswaIPXPZcMVtPGEcxC_Y1lMbSY7R1CSt76n_wbKUEvDvphoeSd8efBiO7BEFudZ19aFJBLBG6RjtwPcMHQe1fYCp_CsfwEBl0",

            };
            bbbSearchProxy = new BBBSearchProxy(bbbSearchConfiguration);
        }

        [Fact]
        public void ReturnSearchOrganizationResponseOnValid()
        {
            var response = bbbSearchProxy.SearchOrganization(new BBBSearchRequest()
            {
                PageSize = 10,
                PageNumber = 2,
                BusinessId = null,
                BBBId = null,
                BBBRating = null,
                IsBBBAccredited = false,
                IsReportable = false,
                OrganizationName = "Capital Alliance",
                PrimaryOrganizationName = null,
                PrimaryCategory = null,
                ContactFirstName = null,
                ContactLastName = null,
                ContactMiddleName = null,
                ContactTitle = null,
                ContactPrefix = null,
                Address = null,
                City = null,
                StateProvince = null,
                PostalCode = null,
                Phone = null,
                BusinessURL = null,
                ReportURL = null,
                OrganizationType = null,
                CollectionId = 0,
                OrganizationLastChanged = null,
                RatingLastChanged = null,
                AccreditationStatusLastChanged = null
            });
            Assert.NotNull(response);
        }
    }
}