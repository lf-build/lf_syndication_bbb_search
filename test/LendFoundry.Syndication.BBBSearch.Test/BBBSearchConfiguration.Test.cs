﻿using LendFoundry.Syndication.BBBSearch.Proxy;
using System;
using Xunit;

namespace LendFoundry.Syndication.BBBSearch.Test
{
    public class BBBSearchConfigurationTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new BBBSearchProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var bbbSearchConfiguration = new BBBSearchConfiguration
            {
                AuthToken = "",
                BaseUrl = "",
                OrganizationSearchUrl = ""
            };

            Assert.Throws<ArgumentNullException>(() => new BBBSearchProxy(bbbSearchConfiguration));

            var bbbSearchConfiguration1 = new BBBSearchConfiguration
            {
                AuthToken = "",
                BaseUrl = "www.xyz.com",
                OrganizationSearchUrl = "www.xyz.com"
            };

            Assert.Throws<ArgumentNullException>(() => new BBBSearchProxy(bbbSearchConfiguration1));

            var bbbSearchConfiguration2 = new BBBSearchConfiguration
            {
                AuthToken = "waetwga4sg656x5e4ra6fa516fgac6ga4sg5ew4c6g5416g",
                BaseUrl = null,
                OrganizationSearchUrl = ""
            };
            Assert.Throws<ArgumentNullException>(() => new BBBSearchProxy(bbbSearchConfiguration2));
        }
    }
}