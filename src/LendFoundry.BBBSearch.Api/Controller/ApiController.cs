﻿#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else

using Microsoft.AspNet.Mvc;

#endif

using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Syndication.BBBSearch.Request;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using LendFoundry.Syndication.BBBSearch.Response;

namespace LendFoundry.BBBSearch.Api.Controller
{
    /// <summary>
    /// Expose endpoint to access BBBSearch service methods
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors

        /// <summary>
        /// Parameterize Constructor
        /// </summary>
        /// <param name="service">Instance of IBBBSearchService</param>
        public ApiController(IBBBSearchService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// IBBBSearchService property
        /// </summary>
        private IBBBSearchService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// End point call the method to Search organization details
        /// </summary>
        /// <param name="entityType">type of Entity</param>
        /// <param name="entityId">entity Id</param>
        /// <param name="request">contain attributes to perform request</param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/bbbsearch/organization")]
#if DOTNET2
        [ProducesResponseType(typeof(IBBBSearchResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SearchOrganization(string entityType, string entityId, [FromBody]BBBSearchRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.SearchOrganization(entityType, entityId, request)));
                }
                catch (BBBSearchException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        #endregion Public Methods
    }
}