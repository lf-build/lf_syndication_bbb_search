﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.BBBSearch.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// ServiceName
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "bbbsearch";
        
    }
}