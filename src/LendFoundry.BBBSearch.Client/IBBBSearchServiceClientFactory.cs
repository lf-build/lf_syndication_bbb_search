﻿using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Security.Tokens;

namespace LendFoundry.BBBSearch.Client
{
    public interface IBBBSearchServiceClientFactory
    {
        #region Public Methods

        IBBBSearchService Create(ITokenReader reader);

        #endregion Public Methods
    }
}