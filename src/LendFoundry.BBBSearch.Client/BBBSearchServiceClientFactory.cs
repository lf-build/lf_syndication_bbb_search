﻿using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.BBBSearch.Client
{
    public class BBBSearchServiceClientFactory : IBBBSearchServiceClientFactory
    {
        #region Public Constructors

        [Obsolete("Need to use the overloaded with Uri")]
        public BBBSearchServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public BBBSearchServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        #endregion Private Properties

        #region Public Methods

        public IBBBSearchService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("bbbsearch");
            }
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new BBBSearchService(client);
        }

        #endregion Public Methods
    }
}
