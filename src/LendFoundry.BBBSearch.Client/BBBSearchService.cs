﻿using LendFoundry.Syndication.BBBSearch;
using LendFoundry.Syndication.BBBSearch.Request;
using LendFoundry.Syndication.BBBSearch.Response;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.BBBSearch.Client
{
    /// <summary>
    /// class used for injecting IBBBSearch service.
    /// </summary>
    public class BBBSearchService : IBBBSearchService
    {
        #region Public Constructors

        public BBBSearchService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IBBBSearchResponse> SearchOrganization(string entityType, string entityId, IBBBSearchRequest requestParam)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/bbbsearch/organization", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(requestParam);
            return await Client.ExecuteAsync<BBBSearchResponse>(request);
        }

        #endregion Public Methods
    }
}