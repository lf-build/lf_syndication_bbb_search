﻿using LendFoundry.Security.Tokens;

using Microsoft.Extensions.DependencyInjection;
using System;

namespace LendFoundry.BBBSearch.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class BBBSearchServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddBBBSearchService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IBBBSearchServiceClientFactory>(p => new BBBSearchServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IBBBSearchServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        #endregion Public Methods
    }
}