﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Response
{
    public class SearchResult : ISearchResult
    {
        #region Public Constructors

        public SearchResult()
        {
        }

        public SearchResult(Proxy.ISearchResult searchResult)
        {
            if (searchResult != null)
            {
                BusinessId = searchResult.BusinessId;
                OrganizationName = searchResult.OrganizationName;
                PrimaryCategory = searchResult.PrimaryCategory;
                ReportURL = searchResult.ReportURL;
                City = searchResult.City;
                StateProvince = searchResult.StateProvince;
                Phones = searchResult.Phones;
                BusinessURLs = searchResult.BusinessURLs;
                Address = searchResult.Address;
                AltOrganizationNames = searchResult.AltOrganizationNames;
                OrganizationType = searchResult.OrganizationType;
                OrganizationLastChanged = searchResult.OrganizationLastChanged;
                RatingLastChanged = searchResult.RatingLastChanged;
                AccreditationStatusLastChanged = searchResult.AccreditationStatusLastChanged;
                if (searchResult.RatingIcons != null)
                    CopyRatingIconList(searchResult.RatingIcons);
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public object AccreditationStatusLastChanged { get; set; }
        public string Address { get; set; }
        public IList<string> AltOrganizationNames { get; set; }
        public string BusinessId { get; set; }
        public IList<string> BusinessURLs { get; set; }
        public string City { get; set; }
        public DateTime OrganizationLastChanged { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationType { get; set; }
        public IList<string> Phones { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryCategory { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IRatingIcon, RatingIcon>))]
        public IList<IRatingIcon> RatingIcons { get; set; }

        public DateTime? RatingLastChanged { get; set; }
        public string ReportURL { get; set; }
        public string StateProvince { get; set; }

        #endregion Public Properties

        #region Private Methods

        private void CopyRatingIconList(IList<Proxy.IRatingIcon> ratingIconList)
        {
            RatingIcons = new List<IRatingIcon>();
            foreach (var item in ratingIconList)
            {
                RatingIcons.Add(new RatingIcon(item));
            }
        }

        #endregion Private Methods
    }
}