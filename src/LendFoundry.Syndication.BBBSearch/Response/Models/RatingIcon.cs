﻿namespace LendFoundry.Syndication.BBBSearch.Response
{
    public class RatingIcon : IRatingIcon
    {
        #region Public Constructors

        public RatingIcon()
        {
        }

        public RatingIcon(Proxy.IRatingIcon ratingIcon)
        {
            if (ratingIcon != null)
            {
                DeviceType = ratingIcon.DeviceType;
                Url = ratingIcon.Url;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public string DeviceType { get; set; }

        public string Url { get; set; }

        #endregion Public Properties
    }
}