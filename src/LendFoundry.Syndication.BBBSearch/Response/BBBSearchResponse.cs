﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Response
{
    public class BBBSearchResponse : IBBBSearchResponse
    {
        #region Public Constructors

        public BBBSearchResponse()
        {
        }

        public BBBSearchResponse(Proxy.IBBBSearchResponse bbbSearchResponse)
        {
            if (bbbSearchResponse != null)
            {
                TotalResults = bbbSearchResponse.TotalResults;
                PageNumber = bbbSearchResponse.PageNumber;
                if (bbbSearchResponse.SearchResults != null)
                    CopySearchResultList(bbbSearchResponse.SearchResults);
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public int PageNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISearchResult, SearchResult>))]
        public IList<ISearchResult> SearchResults { get; set; }

        public int TotalResults { get; set; }

        #endregion Public Properties

        #region Private Methods

        private void CopySearchResultList(IList<Proxy.ISearchResult> searchResultList)
        {
            SearchResults = new List<ISearchResult>();
            foreach (var item in searchResultList)
            {
                SearchResults.Add(new SearchResult(item));
            }
        }

        #endregion Private Methods
    }
}