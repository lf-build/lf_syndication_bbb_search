﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.BBBSearch {
    /// <summary>
    /// BBBSearchConfiguration class
    /// </summary>
    public class BBBSearchConfiguration : IBBBSearchConfiguration, IDependencyConfiguration {
        #region Public Properties

        public string AuthToken { get; set; }
        public string BaseUrl { get; set; }
        public string OrganizationSearchUrl { get; set; }
        public bool UseProxy { get; set; } = true;
        public string ProxyUrl { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

        #endregion Public Properties
    }
}