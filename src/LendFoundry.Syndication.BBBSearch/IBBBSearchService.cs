﻿using LendFoundry.Syndication.BBBSearch.Request;
using LendFoundry.Syndication.BBBSearch.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.BBBSearch
{
    public interface IBBBSearchService
    {
        #region Public Methods

        Task<IBBBSearchResponse> SearchOrganization(string entityType, string entityId, IBBBSearchRequest requestParam);

        #endregion Public Methods
    }
}