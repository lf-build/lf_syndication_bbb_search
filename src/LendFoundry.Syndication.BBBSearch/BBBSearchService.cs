﻿using LendFoundry.Syndication.BBBSearch.Events;
using LendFoundry.Syndication.BBBSearch.Proxy;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;


namespace LendFoundry.Syndication.BBBSearch
{
    public class BBBSearchService : IBBBSearchService
    {
        #region Public Constructors

        public BBBSearchService(IBBBSearchProxy proxy, IEventHubClient eventHub, ILookupService lookup, ILogger logger, ITenantTime tenantTime)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            TenantTime = tenantTime;
        }

        #endregion Public Constructors

        #region Private Properties

        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private IBBBSearchProxy Proxy { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }

        #endregion Private Properties

        #region Public Methods

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        public async Task<Response.IBBBSearchResponse> SearchOrganization(string entityType, string entityId, Request.IBBBSearchRequest requestParam)
        {
            Logger.Info("Started Execution for SearchOrganization Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: BBBSearch");

            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));

            if (requestParam == null)
                throw new ArgumentNullException(nameof(requestParam));

            if (string.IsNullOrWhiteSpace(requestParam.OrganizationName))
                throw new ArgumentNullException(nameof(requestParam.OrganizationName));
            try
            {
                var proxyRequest = new BBBSearchRequest(requestParam);
                var response = Proxy.SearchOrganization(proxyRequest);
                var result = new Response.BBBSearchResponse(response);
                await EventHub.Publish(new BBBSearchSearchOrganizationRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = requestParam,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for SearchOrganization Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Syndication: BBBSearch");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SearchOrganization Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Syndication: BBBSearch" + "Exception" + exception.Message);

                await EventHub.Publish(new BBBSearchSearchOrganizationRequestFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = requestParam,
                    ReferenceNumber = null
                });
                throw;
            }
        }

        #endregion Public Methods
    }
}