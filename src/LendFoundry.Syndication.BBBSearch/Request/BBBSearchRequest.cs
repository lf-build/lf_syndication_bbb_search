﻿using System;

namespace LendFoundry.Syndication.BBBSearch.Request
{
    public class BBBSearchRequest : IBBBSearchRequest
    {
        #region Public Properties

        public DateTime? AccreditationStatusLastChanged { get; set; }
        public string Address { get; set; }
        public string BBBId { get; set; }
        public string BBBRating { get; set; }
        public string BusinessId { get; set; }
        public string BusinessURL { get; set; }
        public string City { get; set; }
        public int CollectionId { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactMiddleName { get; set; }
        public string ContactPrefix { get; set; }
        public string ContactTitle { get; set; }
        public bool IsBBBAccredited { get; set; }
        public bool IsReportable { get; set; }
        public DateTime? OrganizationLastChanged { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationType { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryCategory { get; set; }
        public string PrimaryOrganizationName { get; set; }
        public DateTime? RatingLastChanged { get; set; }
        public string ReportURL { get; set; }
        public string StateProvince { get; set; }

        #endregion Public Properties
    }
}