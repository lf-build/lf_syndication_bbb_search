﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.BBBSearch
{
    [Serializable]
    public class BBBSearchException : Exception
    {
        #region Public Constructors

        public BBBSearchException()
        {
        }

        public BBBSearchException(string message) : this(null, message, null)
        {
        }

        public BBBSearchException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public BBBSearchException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        public BBBSearchException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        #endregion Public Constructors

        #region Protected Constructors

        protected BBBSearchException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        public string ErrorCode { get; set; }

        #endregion Public Properties
    }
}