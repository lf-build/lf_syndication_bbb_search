﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.BBBSearch {
    public interface IBBBSearchConfiguration : IDependencyConfiguration {
        #region Public Properties

        string AuthToken { get; set; }
        string BaseUrl { get; set; }
        string OrganizationSearchUrl { get; set; }
        bool UseProxy { get; set; }
        string ProxyUrl { get; set; }
        int PageSize { get; set; }
        int PageNumber { get; set; }
        string ConnectionString { get; set; }
        #endregion Public Properties
    }
}