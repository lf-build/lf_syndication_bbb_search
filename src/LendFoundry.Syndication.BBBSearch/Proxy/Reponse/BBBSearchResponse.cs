﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public class BBBSearchResponse : IBBBSearchResponse
    {
        #region Public Properties

        public int PageNumber { get; set; }

        [JsonConverter(typeof(ConcreteListJsonConverter<List<SearchResult>, ISearchResult>))]
        public IList<ISearchResult> SearchResults { get; set; }

        public int TotalResults { get; set; }

        #endregion Public Properties
    }
}