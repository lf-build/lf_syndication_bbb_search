﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public interface IBBBSearchResponse
    {
        #region Public Properties

        int PageNumber { get; set; }
        IList<ISearchResult> SearchResults { get; set; }
        int TotalResults { get; set; }

        #endregion Public Properties
    }
}