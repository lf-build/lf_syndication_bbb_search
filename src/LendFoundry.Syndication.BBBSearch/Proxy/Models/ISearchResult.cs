﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public interface ISearchResult
    {
        #region Public Properties

        object AccreditationStatusLastChanged { get; set; }
        string Address { get; set; }
        IList<string> AltOrganizationNames { get; set; }
        string BusinessId { get; set; }
        IList<string> BusinessURLs { get; set; }
        string City { get; set; }
        DateTime OrganizationLastChanged { get; set; }
        string OrganizationName { get; set; }
        string OrganizationType { get; set; }
        IList<string> Phones { get; set; }
        string PostalCode { get; set; }
        string PrimaryCategory { get; set; }
        IList<IRatingIcon> RatingIcons { get; set; }
        DateTime? RatingLastChanged { get; set; }
        string ReportURL { get; set; }
        string StateProvince { get; set; }

        #endregion Public Properties
    }
}