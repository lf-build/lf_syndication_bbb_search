﻿namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public class RatingIcon : IRatingIcon
    {
        #region Public Properties

        public string DeviceType { get; set; }

        public string Url { get; set; }

        #endregion Public Properties
    }
}