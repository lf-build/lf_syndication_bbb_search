﻿namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public interface IRatingIcon
    {
        #region Public Properties

        string DeviceType { get; set; }
        string Url { get; set; }

        #endregion Public Properties
    }
}