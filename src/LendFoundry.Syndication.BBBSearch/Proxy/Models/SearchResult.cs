﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public class SearchResult : ISearchResult
    {
        #region Public Properties

        public object AccreditationStatusLastChanged { get; set; }
        public string Address { get; set; }
        public IList<string> AltOrganizationNames { get; set; }
        public string BusinessId { get; set; }
        public IList<string> BusinessURLs { get; set; }
        public string City { get; set; }
        public DateTime OrganizationLastChanged { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationType { get; set; }
        public IList<string> Phones { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryCategory { get; set; }

        [JsonConverter(typeof(ConcreteListJsonConverter<List<RatingIcon>, IRatingIcon>))]
        public IList<IRatingIcon> RatingIcons { get; set; }

        public DateTime? RatingLastChanged { get; set; }
        public string ReportURL { get; set; }
        public string StateProvince { get; set; }

        #endregion Public Properties
    }
}