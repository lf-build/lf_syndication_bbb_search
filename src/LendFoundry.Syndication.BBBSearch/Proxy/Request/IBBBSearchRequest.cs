﻿using System;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public interface IBBBSearchRequest
    {
        #region Public Properties

        DateTime? AccreditationStatusLastChanged { get; set; }
        string Address { get; set; }
        string BBBId { get; set; }
        string BBBRating { get; set; }
        string BusinessId { get; set; }
        string BusinessURL { get; set; }
        string City { get; set; }
        int CollectionId { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        string ContactMiddleName { get; set; }
        string ContactPrefix { get; set; }
        string ContactTitle { get; set; }
        bool IsBBBAccredited { get; set; }
        bool IsReportable { get; set; }
        DateTime? OrganizationLastChanged { get; set; }
        string OrganizationName { get; set; }
        string OrganizationType { get; set; }
        int PageNumber { get; set; }
        int PageSize { get; set; }
        string Phone { get; set; }
        string PostalCode { get; set; }
        string PrimaryCategory { get; set; }
        string PrimaryOrganizationName { get; set; }
        DateTime? RatingLastChanged { get; set; }
        string ReportURL { get; set; }
        string StateProvince { get; set; }

        #endregion Public Properties
    }
}