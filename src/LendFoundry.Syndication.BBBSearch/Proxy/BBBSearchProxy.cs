﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Reflection;

namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public class BBBSearchProxy : IBBBSearchProxy
    {
        #region Public Constructors

        public BBBSearchProxy(IBBBSearchConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.BaseUrl))
                throw new ArgumentNullException(nameof(configuration.BaseUrl));
            if (string.IsNullOrWhiteSpace(configuration.OrganizationSearchUrl))
                throw new ArgumentNullException(nameof(configuration.OrganizationSearchUrl));
            if (string.IsNullOrWhiteSpace(configuration.AuthToken))
                throw new ArgumentNullException(nameof(configuration.AuthToken));
            Configuration = configuration;
        }

        #endregion Public Constructors

        #region Private Properties

        private IBBBSearchConfiguration Configuration { get; }

        #endregion Private Properties

        #region Public Methods

        public IBBBSearchResponse SearchOrganization(BBBSearchRequest requestParam)
        {
            if (requestParam == null)
                throw new ArgumentNullException(nameof(requestParam));
            var baseUri = new Uri(Configuration.BaseUrl);
            var uri = Configuration.UseProxy ? new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}") : baseUri;
            var client = new RestClient(uri);
            IRestRequest request = new RestRequest(Configuration.OrganizationSearchUrl.TrimEnd('/'), Method.GET);
            request.AddHeader("Host", baseUri.Host);
            requestParam.PageNumber = Configuration.PageNumber;
            requestParam.PageSize = Configuration.PageSize;
            AddParamToRequest(ref request, requestParam);
            return ExecuteRequest<BBBSearchResponse>(client, request);
        }

        #endregion Public Methods

        #region Private Methods

        private void AddParamToRequest<T>(ref IRestRequest request, T model) where T : class
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            PropertyInfo[] propertiesList = typeof(T).GetProperties();
            foreach (var prop in propertiesList)
            {
                if (prop.GetValue(model) != null)
                {
                    if (prop.PropertyType == typeof(int) && !prop.GetValue(model).Equals(0))
                    {
                        request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                    if (prop.PropertyType == typeof(string) && !string.IsNullOrWhiteSpace(prop.GetValue(model).ToString()))
                    {
                        request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                    if (prop.PropertyType == typeof(bool) && prop.GetValue(model).Equals(true))
                    {
                        request.AddParameter(prop.Name, prop.GetValue(model));
                    }
                }
            }
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Authorization", $"Bearer {Configuration.AuthToken}");
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new BBBSearchException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new BBBSearchException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new BBBSearchException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new BBBSearchException(response.ErrorMessage);

            if (response.StatusCode == HttpStatusCode.NoContent)
                return null;

            if (response.StatusCode != HttpStatusCode.OK)
                throw new BBBSearchException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.ErrorMessage ?? ""}");
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.ErrorMessage, exception);
            }
        }

        #endregion Private Methods
    }
}