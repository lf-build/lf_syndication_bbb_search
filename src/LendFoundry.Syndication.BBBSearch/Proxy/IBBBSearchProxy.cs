﻿namespace LendFoundry.Syndication.BBBSearch.Proxy
{
    public interface IBBBSearchProxy
    {
        #region Public Methods

        IBBBSearchResponse SearchOrganization(BBBSearchRequest requestParam);

        #endregion Public Methods
    }
}